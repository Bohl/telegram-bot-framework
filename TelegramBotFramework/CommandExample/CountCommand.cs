﻿using System;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace TelegramBotFramework.CommandExample {
    class CountCommand : Command {
        private int count;
        public CountCommand(String name)
            : base(name) {
            count = 0;
        }

        public override bool executeCommand(Message message) {
            Client.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);

            Client.SendTextMessageAsync(message.Chat.Id, "Qtt to add:");
            return false;
        }

        public override bool matches(MessageType type, String message) {
            if(type == MessageType.TextMessage) {
                if (message.Equals(this.Name))
                    return true;
            }
            return false;
        }

        public override bool nextAction(Message message) {
            String number = message.Text;
            try {
                count += int.Parse(number);
                Client.SendTextMessageAsync(message.Chat.Id, "Count: " + count);
                return true;
            } catch {
                Client.SendTextMessageAsync(message.Chat.Id, "That's not a number! Try again:");
                return false;
            }
        }

        public override bool callbackAction(CallbackQuery data) {
            return false;
        }

        public override void abort() {
            return;
        }
    }
}
