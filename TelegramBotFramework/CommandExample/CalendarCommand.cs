﻿using System;
using System.Globalization;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace TelegramBotFramework.CommandExample {
    class CalendarCommand : Command {
        private int month, year;
        public CalendarCommand(string name)
            : base(name) {
            month = DateTime.Now.Month;
            year = DateTime.Now.Year;
        }

        public override bool executeCommand(Message message) {
            var calendar = createCalendar(month, year);
            Client.SendTextMessageAsync(message.Chat.Id, "Pick a day:",
                replyMarkup: calendar);
            return false;
        }

        public override bool matches(MessageType type, string message) {
            if (type == MessageType.TextMessage) {
                if (message.Equals(this.Name))
                    return true;
            }
            return false;
        }

        public override bool nextAction(Message message) {
            return false;
        }

        public override bool callbackAction(CallbackQuery data) {
            int response = callbackResponseToInt(data.Data);
            switch (response) {
                case 0: //Day picked
                    Client.AnswerCallbackQueryAsync(data.Id, $"Picked: {data.Data}");
                    return true;
                case 1: //Prev
                    month--;
                    if(month < 1) {
                        month = 12;
                        year--;
                    }
                    var prevMonth = createCalendar(month, year);
                    Client.EditMessageTextAsync(data.Message.Chat.Id, data.Message.MessageId, "Pick a day:", replyMarkup: prevMonth);
                    Client.AnswerCallbackQueryAsync(data.Id, "");
                    return false;
                case 2: //Next
                    month++;
                    if(month > 12) {
                        month = 1;
                        year++;
                    }
                    var nextMonth = createCalendar(month, year);
                    Client.EditMessageTextAsync(data.Message.Chat.Id, data.Message.MessageId, "Pick a day:", replyMarkup: nextMonth);
                    Client.AnswerCallbackQueryAsync(data.Id, "");
                    return false;
                default: //Ignore
                    Client.AnswerCallbackQueryAsync(data.Id, "");
                    return false;
            }
            
        }

        public override void abort() {
            return;
        }

        private InlineKeyboardMarkup createCalendar(int month, int year) {
            DateTime date = new DateTime(year, month, 1);
            String m = date.ToString("MMM", CultureInfo.InvariantCulture);

            int weeks = MondaysInMonth(date);
            InlineKeyboardButton[][] calendar = new InlineKeyboardButton[3 + weeks][];
            calendar[0] = new InlineKeyboardButton[1];
            calendar[0][0] = new InlineKeyboardButton(m, "ignore");
            calendar[1] = new InlineKeyboardButton[7];
            calendar[1][0] = new InlineKeyboardButton("Mo", "ignore");
            calendar[1][1] = new InlineKeyboardButton("Tu", "ignore");
            calendar[1][2] = new InlineKeyboardButton("We", "ignore");
            calendar[1][3] = new InlineKeyboardButton("Th", "ignore");
            calendar[1][4] = new InlineKeyboardButton("Fr", "ignore");
            calendar[1][5] = new InlineKeyboardButton("Sa", "ignore");
            calendar[1][6] = new InlineKeyboardButton("Su", "ignore");

            int week = 2;
            calendar[week] = new InlineKeyboardButton[7];
            int daysInMonth = DateTime.DaysInMonth(date.Year, date.Month);
            for (int i = 1; i <= daysInMonth; i++) {
                DateTime d = new DateTime(date.Year, date.Month, i);
                int day = ((int) d.DayOfWeek == 0) ? 6 : (int) d.DayOfWeek - 1;
                if (i == 1) {
                    int blanks = 0;
                    while (blanks < day) {
                        calendar[week][blanks] = new InlineKeyboardButton(".", "ignore");
                        blanks++;
                    }
                }
                calendar[week][day] = new InlineKeyboardButton(d.ToString("dd"), d.ToString("dd-MM-yyyy"));
                if (day == 6) {
                    week++;
                    calendar[week] = new InlineKeyboardButton[7];
                }
                if (i == daysInMonth) {
                    day++;
                    while (day < 7) {
                        calendar[week][day] = new InlineKeyboardButton(".", "ignore");
                        day++;
                    }
                }
            }
            calendar[calendar.Length - 1] = new InlineKeyboardButton[3];
            calendar[calendar.Length - 1][0] = new InlineKeyboardButton("<", "prev");
            calendar[calendar.Length - 1][1] = new InlineKeyboardButton(date.ToString("yyyy"), "ignore");
            calendar[calendar.Length - 1][2] = new InlineKeyboardButton(">", "next");

            var keyboard = new InlineKeyboardMarkup(calendar);

            return keyboard;
        }

        private static int callbackResponseToInt(String data) {
            if (data.Equals("ignore")) {
                return -1;
            }else if(data.Equals("prev")) {
                return 1;
            } else if(data.Equals("next")) {
                return 2;
            } else {
                return 0;
            }

        }

        private static int MondaysInMonth(DateTime time) {
            //extract the month
            int daysInMonth = DateTime.DaysInMonth(time.Year, time.Month);
            var firstOfMonth = new DateTime(time.Year, time.Month, 1);
            //days of week starts by default as Sunday = 0
            var firstDayOfMonth = ((int) firstOfMonth.DayOfWeek == 0) ? 6 : (int) firstOfMonth.DayOfWeek - 1;
            var weeksInMonth = (int) Math.Ceiling((firstDayOfMonth + daysInMonth) / 7.0);

            return weeksInMonth;
        }
    }
}
