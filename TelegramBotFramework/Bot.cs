﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using TelegramBotFramework.Exceptions;

namespace TelegramBotFramework {
    class Bot {
        public String Token { get; set; }
        private Dictionary<String, Command> commands;
        private readonly TelegramBotClient botClient;
        private Dictionary<long, Command> commandInExecution;

        public Bot(String token) {
            this.Token = token;
            botClient = new TelegramBotClient(this.Token);
            commands = new Dictionary<String, Command>();
            commandInExecution = new Dictionary<long, Command>();
        }

        public void loadCommand(Command command) {
            if(!commands.ContainsKey(command.Name)) {
                commands[command.Name] = command;
                command.Client = botClient;
                Console.WriteLine("Command added: " + command.Name + ".");
            } else {
                throw new CommandAlreadyExistsException(command.Name + ": Command already exists.");
            }
        }

        public void start() {
            botClient.OnMessage += OnMessageReceived;
            botClient.OnMessageEdited += OnMessageReceived;
            botClient.OnCallbackQuery += OnCallbackReceived;

            var me = botClient.GetMeAsync().Result;

            Console.Title = me.Username;

            botClient.StartReceiving();
            Console.WriteLine("Bot started.");
        }

        public void stop() {
            botClient.StopReceiving();
            Console.WriteLine("Bot stopped.");
        }

        private void OnMessageReceived(Object sender, MessageEventArgs args) {
            Message message = args.Message;

            bool finished;
            if(commandInExecution.ContainsKey(message.Chat.Id)) {
                Command c =  getCommand(message.Type, message.Text);
                if (c != null) {
                    commandInExecution[message.Chat.Id].abort();
                    Console.WriteLine(commandInExecution[message.Chat.Id].Name + " aborted.");
                    commandInExecution[message.Chat.Id] = c;
                    finished = commandInExecution[message.Chat.Id].executeCommand(message);
                } else {
                    finished = commandInExecution[message.Chat.Id].nextAction(message);
                }
            } else {
                commandInExecution[message.Chat.Id] = getCommand(message.Type, message.Text);
                if (commandInExecution[message.Chat.Id] != null) {
                    finished = commandInExecution[message.Chat.Id].executeCommand(message);
                } else {
                    finished = true;
                }
            }
            if (finished)
                commandInExecution.Remove(message.Chat.Id);
            Console.WriteLine("Finished: " + finished.ToString() + ". " + message.Type + " from " + message.From.Username + ": " + message.Text);
        }

        private void OnCallbackReceived(Object sender, CallbackQueryEventArgs args) {
            CallbackQuery data = args.CallbackQuery;
            dynamic parsedData = JsonConvert.DeserializeObject(data.Data);
            Command c = getCommand(MessageType.TextMessage, (String)parsedData.type);
            c.callbackAction(data);
            Console.WriteLine("Callback from" + data.From.Username + ":  " + data.Data);
        }

        private Command getCommand(MessageType type, String message) {
            foreach(KeyValuePair<String, Command> kvp in commands) {
                if(kvp.Value.matches(type, message)) {
                    return kvp.Value;
                }
            }
            return null;
        }
    }
}
