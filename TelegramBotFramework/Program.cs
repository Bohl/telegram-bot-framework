﻿using System;
using TelegramBotFramework.CommandExample;

namespace TelegramBotFramework {
    class Program {
        static void Main(string[] args) {
            Bot bot = new Bot("Your API Key here");
            bot.loadCommand(new CountCommand("/count"));
            bot.loadCommand(new CalendarCommand("/calendar"));
            bot.start();
            Console.ReadLine();
            bot.stop();
        }
    }
}
