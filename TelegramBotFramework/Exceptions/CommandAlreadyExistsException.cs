﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelegramBotFramework.Exceptions {
    [Serializable]
    class CommandAlreadyExistsException : Exception {
        public CommandAlreadyExistsException() { }

        public CommandAlreadyExistsException(String message) 
            : base(message) { 
        }

        public CommandAlreadyExistsException(String message, Exception innerException)
            : base(message, innerException) {
        }
    }
}
