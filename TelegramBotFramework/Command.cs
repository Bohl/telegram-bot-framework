﻿using System;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace TelegramBotFramework {
    abstract class Command {
        public String Name { get; }
        public TelegramBotClient Client { get; set; }
        public Command(String name) {
            this.Name = name;
        }

        public abstract bool executeCommand(Message message);
        public abstract bool nextAction(Message message);
        public abstract bool callbackAction(CallbackQuery data);
        public abstract bool matches(MessageType type, String message);
        public abstract void abort();
    }
}
